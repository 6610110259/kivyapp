from kivy.config import Config
from kivy.core.window import Window
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.clock import Clock
from kivy.uix.textinput import TextInput
from kivy.core.audio import SoundLoader

import random

Config.set('graphics', 'width', '800')
Config.set('graphics', 'height', '600')

class Target(Image):
    def __init__(self, game, **kwargs):
        super(Target, self).__init__(**kwargs)
        self.source = 'images/aim.png'
        self.size = (50, 50)
        self.game = game
        self.new_target_position()

    def on_touch_down(self, touch):
        try:
            if self.collide_point(*touch.pos):
                self.game.increment_score()
                self.new_target_position()
                self.game.play_sound()
        except Exception as e:
            print(f"Error in on_touch_down: {e}")

    def new_target_position(self):
        pos_x = random.randint(-700, 700)
        pos_y = random.randint(-100, 700)
        self.pos = (pos_x, pos_y)

class AimTrainerGame(BoxLayout):
    def __init__(self, **kwargs):
        super(AimTrainerGame, self).__init__(**kwargs)
        self.orientation = 'vertical'
        self.score = 0
        self.time_remaining = 5000
        self.game_in_progress = False
        self.time_schedule = None
        self.player_name = ""
        self.sound = SoundLoader.load('sound/s1.wav')  # ระบุเส้นทางของไฟล์เสียงของคุณที่นี่
        self.sound_submit = SoundLoader.load('sound/s2.wav')
        self.sound_setting = SoundLoader.load('sound/s3.wav')
        self.sound_start = SoundLoader.load('sound/s4.wav')
        self.sound_decrease = SoundLoader.load('sound/s5.wav')
        self.sound_increase = SoundLoader.load('sound/s6.wav')
        self.sound_game_over = SoundLoader.load('sound/s7.wav')

        self.input_user()

    def play_sound(self):
        if self.sound:
            self.sound.play()

    def input_user(self):
            with self.canvas:
                self.background = Image(source='images/bg1.jpg', allow_stretch=True, keep_ratio=False)
                self.bind(size=self.background.setter('size'))
                self.bind(pos=self.background.setter('pos'))

            self.player_name_input = TextInput(multiline=False, hint_text='Enter player name',font_size=65, size_hint=(0.5, 0), pos_hint={"center_x":0.5})
            # self.add_widget(self.player_name_input)
            box2 = BoxLayout()
            # self.add_widget(box)

            self.submit_button = Button(text="Submit", on_press=self.create_widgets,font_size=35, size_hint=(None, None), size=(200, 100), background_color=(1, 0.5, 1, 1))
            # self.add_widget(self.submit_button)
            # self.submit_button.pos_hint = {'center_x': 0.5, 'center_y': 0.5}

            box2.add_widget(self.player_name_input)
            box2.add_widget(self.submit_button)
            self.add_widget(box2)

    def create_widgets(self, instance):
        if self.sound_submit:
            self.sound_submit.play()
        with self.canvas:
            self.background.source = 'images/bg2.jpg'
        
        box = Label(size_hint = (.1, .6))
        self.player_name = self.player_name_input.text
        self.clear_widgets()

        self.player_name_label = Label(text=f"Player: {self.player_name}", font_size=34, size_hint=(.5, .1), color=(0.5, 1, 0.5, 1))
        self.add_widget(self.player_name_label)

        self.score_label = Label(text="Score: 0", font_size=34, size_hint=(.5, .1),color=(0.5, 1, 0.5, 1))
        self.add_widget(self.score_label)

        self.target = None  
        self.add_widget(box)
        self.start_button = Button(text="Start", on_press=self.start_game,font_size=25, size_hint=(None, None), size=(200, 100), background_color=(0.2, 0.9, 0.2, 1))
        self.add_widget(self.start_button)
        self.start_button.pos_hint = {'center_x': 0.5, 'center_y': 0.5}

        self.settings_button = Button(text="Settings", on_press=self.open_settings,font_size=25, size_hint=(None, None), size=(200, 100), background_color=(0.9, 0.9, 0.1, 1))
        self.add_widget(self.settings_button)
        self.settings_button.pos_hint = {'center_x': 0.5, 'center_y': 0.5}

        self.time_label = Label(text=f"Time: {self.time_remaining / 1000:.3f}", font_size=34, color=(0.6, 0.9, 0.4, 1))
        self.add_widget(self.time_label)

    def start_game(self, instance):
        if self.sound_start:
            self.sound_start.play()
        with self.canvas:
            self.background.source = 'images/bg3.jpg'

        if not self.game_in_progress:
            self.player_name = self.player_name_input.text
            self.score = 0
            self.update_score_label()
            self.game_in_progress = True
            self.time_schedule = Clock.schedule_interval(self.update_time_label, 1.0 / 60.0)

            # Hide TextInput and show Label
            self.player_name_input.opacity = 0
            self.player_name_label.text = f"Player: {self.player_name}"
            self.player_name_label.opacity = 1

            # Remove this line since self.input_layout is not defined in your code
            # self.input_layout.pos_hint = {'top': 1, 'right': 1}

            self.remove_widget(self.start_button)
            self.remove_widget(self.settings_button)

            self.create_target()
            self.update_time_label(0)


    def open_settings(self, instance):
        if self.sound_setting:
            self.sound_setting.play()
        with self.canvas:
            self.background.source = 'images/bg4.jpg'
        self.clear_widgets()

        self.time_label = Label(text=f"Time: {self.time_remaining / 1000:.3f}", font_size=34, color=(0.6, 0.9, 0.4, 1))
        self.add_widget(self.time_label)
        box3 = BoxLayout()
        self.add_widget(box3)
        # Show the "Increase Time" and "Decrease Time" buttons
        self.increase_time_button = Button(text="Increase Time", on_press=self.increase_time,font_size=25, size_hint=(None, None), size=(200, 100), background_color=(0.1, 0.8, 0.3, 1))
        self.add_widget(self.increase_time_button)
        self.increase_time_button.pos_hint = {'center_x': 0.5, 'center_y': 0.2}

        self.decrease_time_button = Button(text="Decrease Time", on_press=self.decrease_time,font_size=25, size_hint=(None, None), size=(200, 100), background_color=(0.9, 0.1, 0.1, 1))
        self.add_widget(self.decrease_time_button)
        self.decrease_time_button.pos_hint = {'center_x': 0.5, 'center_y': 0.5}

        # Add the "Back" button
        self.back_button = Button(text="Back", on_press=self.back_to_menu,font_size=25, size_hint=(None, None), size=(200, 100), background_color=(0.9, 0.9, 0.9, 1))
        self.add_widget(self.back_button)
        self.back_button.pos_hint = {'center_x': 0.5, 'center_y': 0.8}

    def back_to_menu(self, instance):
        if hasattr(self, 'time_schedule') and self.time_schedule:  # Add this condition
            self.time_schedule.cancel()
        self.create_widgets(None)

    def increase_time(self, instance):
        if self.sound_increase:
            self.sound_increase.play()
        self.time_remaining += 1000
        self.update_time_label(0)

    def decrease_time(self, instance):
        if self.sound_decrease:
            self.sound_decrease.play()
        self.time_remaining = max(0, self.time_remaining - 1000)
        self.update_time_label(0)

    def update_score_label(self):
        self.score_label.text = f"Score: {self.score}"

    def update_time_label(self, dt):
        self.time_remaining -= dt * 1000
        if self.time_remaining <= 0:
            Clock.unschedule(self.update_time_label)
            self.game_in_progress = False
            self.show_score()
        else:
            self.time_label.text = f"Time: {self.time_remaining / 1000:.3f}"

    def increment_score(self):
        self.score += 1
        self.update_score_label()

    def create_target(self):
        self.target = Target(self)
        self.add_widget(self.target)

    def show_score(self):
        if self.sound_game_over:
            self.sound_game_over.play()
        with self.canvas:
            self.background.source = 'images/bg5.jpg'
        self.clear_widgets()
        score_label = Label(text=f"Final Score: {self.score}", font_size=44, color=(1, 0.4, 1, 1))
        restart_button = Button(text="Restart", on_press=self.restart_game,font_size=25, size_hint=(None, None),pos_hint = {'center_x': 0.5} ,size=(200, 100), background_color=(0.3, 0.9, 0.4, 1))
        self.add_widget(score_label)
        self.add_widget(restart_button)

    def reset_game(self):
        self.score = 0
        self.time_remaining = 5000
        self.game_in_progress = False
        self.player_name = ""
        self.clear_widgets()
        # self.input_user()

    def restart_game(self, instance):
        self.reset_game()
        self.create_widgets(None)

class AimTrainerApp(App):   
    def build(self):
        return AimTrainerGame()

if __name__ == '__main__':
    AimTrainerApp().run()
